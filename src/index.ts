const buttons = Array.from(document.getElementsByClassName("move-btn"));

buttons.forEach((btn) =>
    btn.addEventListener("click", (e) => {
        if (e.target instanceof HTMLElement) {
            let move: Move;
            switch (e.target.id) {
                case "rock":
                    move = Move.Rock;
                    break;
                case "paper":
                    move = Move.Paper;
                    break;
                case "scissors":
                    move = Move.Scissors;
                    break;
                default:
                    return;
            }
            console.log(playRound(move, getComputerMove()));
        }
    })
);

enum Move {
    Rock,
    Paper,
    Scissors,
}

enum State {
    Lost,
    Won,
    Draw,
}

function playRound(userMove: Move, botMove: Move) {
    const result = rockPaperScissors(userMove, botMove);
    switch (result) {
        case State.Lost:
            console.log("You lost!");
            break;
        case State.Won:
            console.log("You won!");
            break;
        case State.Draw:
            console.log("It's a draw!");
            break;
        default:
            break;
    }
}

function getComputerMove(): Move {
    const move = Math.floor(Math.random() * 3);
    switch (move) {
        case Move.Rock:
            return Move.Rock;
        case Move.Paper:
            return Move.Paper;
        case Move.Scissors:
            return Move.Scissors;
    }
    return 0;
}

function rockPaperScissors(userMove: Move, botMove: Move): State {
    if (userMove === botMove) {
        return State.Draw;
    }

    if (
        (userMove === Move.Rock && botMove === Move.Scissors) ||
        (userMove === Move.Paper && botMove === Move.Rock) ||
        (userMove === Move.Scissors && botMove === Move.Paper)
    ) {
        return State.Won;
    } else {
        return State.Lost;
    }
}
